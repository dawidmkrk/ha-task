<?php

use PHPUnit\Framework\TestCase;

use HybridAnalysis\ArrayParser;

class ArrayParserTest extends TestCase
{
    public function arraysToParseDataProvider()
    {
        return [
            ['BbBb', ['Aa', 'aaa', 'aaaaa', 'BbBb', 'Aaaa', 'AaAaAa', 'a']],
            ['foo', ['abc', 'acb', 'bac', 'foo', 'bca', 'cab', 'cba']],
            ['foo', ['abc', 'acb', 'bac', 'foo', 'bca', 'cab', 'cba']],
            ['victor', ['silvia', 'vasili', 'victor']],
            ['Harry Potter', ['Tom Marvolo Riddle', 'I am Lord Voldemort', 'Harry Potter']],
            ['a', ['     ', 'a', ' ']],
            ['a', ['     ', 'a', 'aAa']],
        ];
    }

    /**
     * @dataProvider arraysToParseDataProvider
     */
    public function test_it_finds_not_not_fitting_array($uniqueElement, $array)
    {
        $this->assertEquals($uniqueElement, ArrayParser::findUnique($array));
    }


    public function uniqueCharsDataProvider()
    {
        return [
            ['a', 'aAa'],
            ['b', 'BbBb'],
            ['abc', 'cAbaabC'],
        ];
    }

    /**
     * @dataProvider uniqueCharsDataProvider
     */
    public function test_it_returns_unique_chars($expected, $string)
    {
        $this->assertEquals(
            $expected,
            ArrayParser::getUniqueChars($string)
        );
    }
}
