<?php
/**
 * Created by PhpStorm.
 * User: dawid
 * Date: 04.02.2018
 * Time: 15:25
 */

use PHPUnit\Framework\TestCase;
use HybridAnalysis\StringSorter;

class StringSorterTest extends TestCase
{
    public function arraySortDataProvider()
    {
        return [
            ['Thi1s is2 4a T7est', 'is2 Thi1s T7est 4a'],
            ['thr3e On4 5ive', 'On4 thr3e 5ive'],
        ];
    }

    public function wordValidationDataProvider()
    {
        return [
            [\InvalidArgumentException::class, 'The string has to contain digits', 'Some string without digit'],
            [\InvalidArgumentException::class, 'The string may contain only one digit', 'Som3 string with two d1g1ts'],
        ];
    }


    /**
     * @dataProvider arraySortDataProvider
     */
    public function test_it_sorts_array($sorted, $input)
    {
        $this->assertEquals($sorted, StringSorter::sortByContainedNumbers($input));
    }

    public function test_it_denies_words_with_zeros()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The string must not contain zero digits');

        StringSorter::sortByContainedNumbers('Thi1s is2 4a T0est');
    }

    public function test_it_sanitizes_digits()
    {
        $this->assertEquals(1, StringSorter::extractDigitFromString('Thi1s'));
    }

    /**
     * @dataProvider wordValidationDataProvider
     */
    public function test_it_allows_only_one_digit_in_word($exceptionClass, $exceptionMessage, $inputString)
    {
        $this->expectException($exceptionClass);
        $this->expectExceptionMessage($exceptionMessage);

        StringSorter::extractDigitFromString($inputString);
    }
}
