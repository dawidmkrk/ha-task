<?php
/**
 * Created by PhpStorm.
 * User: dawid
 * Date: 04.02.2018
 * Time: 23:28
 */

use PHPUnit\Framework\TestCase;

use HybridAnalysis\Query;

class QueryTest extends TestCase
{
    public function tests_it_parses_query_string()
    {
        $this->assertEquals(
            ["term" => "value", "term2" => "value2:value test"],
            Query::parseQueryString('term:value term2:"value2:value test"')
        );
    }

    public function test_it_throws_exception_on_retrieving_missing_term()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('provided invalid terms');

        Query::parseSearchQuery(['term', 'term1'], 'term:value term2:"value2" term3:"value value" term4:"value : value : value"');
    }

    public function test_it_throws_exception_when_invalid_query_string_passed()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('invalid query string');

        Query::parseSearchQuery(['term', 'term1'], 'term:value term2: value value term3:"value value" term4:"value : value : value"');
    }
}
