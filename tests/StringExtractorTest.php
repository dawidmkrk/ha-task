<?php
/**
 * Created by PhpStorm.
 * User: dawid
 * Date: 01.02.2018
 * Time: 01:32
 */

use HybridAnalysis\StringExtractor;

class StringExtractorTest extends \PHPUnit\Framework\TestCase
{
    public function stringUpperCaseDataProvider()
    {
        return [
            ['TMP', 'Test Me Please'],
            ['TES', 'TEst me please'],
            ['TMS', 'Tst Me please'],
            ['EMT', 'tEst Me please'],
            ['MTE', 'test Me please'],
            ['ETE', 'test me pleasE'],
            //Multibytes Test
            ['ŻCT', 'ŻółCTest'],
            ['SÖÉ', 'ö!érút, Sétány'],
        ];
    }

    public function stringSplitDataProvider()
    {
        return [
            [['s', 'o', 'm', 'e', ' ', 't', 'e', 'x', 't'], 'some text'],
            //Multibytes Test
            [['Ż', 'ó', 'Ł', 'T', 'k', 'o'], 'ŻóŁTko'],
            [['文', '字', '列'], '文字列']
        ];
    }

    public function uppercaseDetectDataProvider()
    {
        return [
            [true, 'A'],
            [false, 'b'],
            [false, ' '],
            [true, 'Ż'],
            [false, 'ó'],
            [true, 'Ł'],
            [false, '$dd%']
        ];
    }

    public function lowercaseDetectDataProvider()
    {
        return [
            [false, 'A'],
            [true, 'b'],
            [false, ' '],
            [false, 'Ż'],
            [true, 'ó'],
            [false, 'Ł'],
            [false, '$dd%']
        ];
    }

    /**
     * @dataProvider stringUpperCaseDataProvider
     */
    public function test_it_returns_alphanumeric_uppercased_letters($expectedOutput, $inputString)
    {
        $this->assertEquals($expectedOutput, StringExtractor::getUpperCasedLetters($inputString));
    }

    /**
     * @dataProvider stringSplitDataProvider
     */
    public function test_it_splits_string_into_array_of_characters($array, $string)
    {
        $this->assertEquals($array, StringExtractor::splitToArray($string));
    }

    /**
     * @dataProvider uppercaseDetectDataProvider
     */
    public function test_it_detects_uppercase($result, $string)
    {
        $this->assertEquals($result, StringExtractor::isUppercase($string));
    }

    /**
     * @dataProvider lowercaseDetectDataProvider
     */
    public function test_it_detects_lowercase($result, $string)
    {
        $this->assertEquals($result, StringExtractor::isLowercase($string));
    }
}
