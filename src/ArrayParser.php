<?php

declare(strict_types=1);

namespace HybridAnalysis;

class ArrayParser
{
    public static function findUnique(array $array): string
    {
        $processedArray = array_filter(array_map('self::getUniqueChars', $array));

        $valuesCount = array_count_values($processedArray);
        asort($valuesCount);

        $uniqueElementKey = array_search(key($valuesCount), $processedArray);

        return $array[$uniqueElementKey];
    }

    /**
     * @param array $array
     * @return array
     */
    public static function getUniqueChars(string $string): string
    {
        $processedString = mb_convert_case($string, MB_CASE_LOWER);

        $letters = StringExtractor::splitToArray($processedString);
        sort($letters);

        return trim(implode('', array_unique($letters)));
    }
}