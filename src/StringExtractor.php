<?php

declare(strict_types=1);

namespace HybridAnalysis;


class StringExtractor
{
    const STRING_LIMIT = 3;

    public static function getUpperCasedLetters(string $string): string
    {
        $lowerCaseLetters = [];
        $upperCaseLetters = [];

        foreach (self::splitToArray($string) as $char) {
            if (self::isUppercase($char)) {
                $upperCaseLetters[] = $char;
            }

            if (self::isLowercase($char)) {
                $lowerCaseLetters[] = mb_convert_case($char, MB_CASE_UPPER);
            }
        }

        $letters = array_slice(
            array_merge($upperCaseLetters, $lowerCaseLetters),
            0,
            self::STRING_LIMIT
        );

        return implode('', $letters);
    }

    /**
     * @param string $string
     * @return array
     */
    public static function splitToArray(string $string): array
    {
        return preg_split('//u', $string, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * @param string $string
     * @return bool
     */
    public static function isUppercase(string $string): bool
    {
        if (ord($string) < 64) return false;
        return (mb_convert_case($string, MB_CASE_UPPER) === $string);
    }

    /**
     * @param string $string
     * @return bool
     */
    public static function isLowercase(string $string): bool
    {
        if (ord($string) < 64) return false;
        return (mb_convert_case($string, MB_CASE_LOWER) === $string);
    }
}