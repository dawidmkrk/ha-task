<?php
declare(strict_types=1);

namespace HybridAnalysis;


class Query
{
    /**
     * @param array $terms
     * @param string $query
     * @return array
     */
    public static function parseSearchQuery(array $terms, string $query)
    {
        $queryParams = self::parseQueryString($query);

        if(count(array_diff($terms, array_keys($queryParams)))) {
            throw new \InvalidArgumentException('provided invalid terms');
        }

        return $queryParams;
    }

    /**
     * @param string $string
     * @return array
     */
    public static function parseQueryString(string $string): array
    {
        $queryParams = [];

        preg_match_all('/(\w+:\w+)|(\w+:\"[^"]*\")/', $string, $matches);

        $unmatchedString = trim(str_replace($matches[0], '', $string));

        if(strlen($unmatchedString)) {
            throw new \InvalidArgumentException('invalid query string');
        }


        foreach ($matches[0] as $match) {
            $chunks = explode(':', str_replace('"', '', $match));

            $key = array_shift($chunks);
            $value = implode(':', $chunks);

            $queryParams[$key] = $value;

        }

        return $queryParams;
    }
}