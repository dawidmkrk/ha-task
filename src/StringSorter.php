<?php

declare(strict_types=1);

namespace HybridAnalysis;


class StringSorter
{
    /**
     * @param string $string
     * @return string
     */
    public static function sortByContainedNumbers(string $string): string
    {
        if (false !== strpos($string, '0')) {
            throw new \InvalidArgumentException('The string must not contain zero digits');
        }

        $stringsArray = explode(' ', $string);

        usort($stringsArray, function ($first, $second) {
            $first = self::extractDigitFromString($first);
            $second = self::extractDigitFromString($second);

            if ($first == $second) {
                return 0;
            }

            return ($first < $second) ? -1 : 1;
        });

        return implode(' ', $stringsArray);
    }

    /**
     * @param string $string
     * @return int
     */
    public static function extractDigitFromString(string $string): int
    {
        $digit = filter_var($string, FILTER_SANITIZE_NUMBER_INT);

        if (empty($digit)) {
            throw new \InvalidArgumentException('The string has to contain digits');
        } elseif ($digit > 9) {
            throw new \InvalidArgumentException('The string may contain only one digit');
        }

        return (int) $digit;
    }
}