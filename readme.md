

Notes: All classes are located in src directory. Below some notes:

**Challenge #1**

* Method: StringExtractor::getUpperCasedLetters

**Challenge #2**

* Method: StringSorter::sortByContainedNumbers
* I claimed the input string is always correct number of digits (Made a validation for words without digits or 2 digits), in production it should be covered as well.

**Challenge #3**

* Method: ArrayParser::findUnique

**Challenge #4**

* Method: Query::parseSearchQuery